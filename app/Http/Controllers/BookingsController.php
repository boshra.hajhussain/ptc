<?php


namespace App\Http\Controllers;


use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BookingsController  extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //prevent not logged in users from using this controller
        $this->middleware('auth');
    }

    /**
     * Show the add booking interface
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('addBooking');
    }



    /**
     * Get a validator for an incoming booking request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'date' => ['required', 'date'],
            'time' => ['required'],

        ]);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        //get request data (form data)
        $data = $request->all();

        //validate all booking parameteres
        $this->validator( $data)->validate();

        //create new booking for current user
        $booking = Booking::create([
            'date' => $data['date'],
            'time' => $data['time'],
            'patient_id' => Auth::user()->id ,
        ]);

        //redirect user to home page
        return redirect()->route('home');
    }



}
