<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertDataToDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctors', function (Blueprint $table) {
            DB::table('doctors')->insert(
                array(
                    'name' => 'shaza',
                    'specialization' => 'A'
                )
            );
            DB::table('doctors')->insert(
                array(
                    'name' => 'ahmad',
                    'specialization' => 'B'
                )
            );    DB::table('doctors')->insert(
                array(
                    'name' => 'mohammad',
                    'specialization' => 'c'
                )
            );    DB::table('doctors')->insert(
                array(
                    'name' => 'khaled',
                    'specialization' => 'D'
                )
            );    DB::table('doctors')->insert(
                array(
                    'name' => 'mahmmod',
                    'specialization' => 'D'
                )
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctors', function (Blueprint $table) {
            //
        });
    }
}
